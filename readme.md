# Pubmed Key Finder

```
App Author: Brian Dranka
App Managers: Ayush Praveen & Richa Mudgal 
```

Pubmed Key Finder is a tool that can be used to search for pubmed papers and fetch key words based on the queried terms. Their can be many use cases of the application. 


## Changes in Version 2.2
1. The application works in two steps now. First to collect results and second to process it. 
2. Users can select the span of records now using radio buttons. 
3. Pubmed API key has been added to the app for placing more queries per second. 
4. Plot title update issue has been solved
5. A basic stop word and number removal system has been added to enhance the relevance of the results. 
6. The application currently displays results from the top 5000 papers only to limit the load on application, In further updates the processing prowess will be improved and it should be able to work on more papers. 

## Changes in Version 2.1 (in comparison with the base application)

1. The application now only works when the submit button is clicked.
2. The search term can now be updated and doesn't hang when the new term is searched. 
3. A basic loader updates the user that the process is running in the background when the button is clicked.
4. Application's reactivity has been enhanced. 